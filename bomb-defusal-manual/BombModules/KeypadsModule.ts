﻿class Symbols {
    static copyright = "\u00A9";
    static arabicTehRing = "\u067C";
    static blackStar = "\u2605";
    static copticCapitalShima = "\u03EC";
    static cyrillicCapitalAbkhasianHa = "\u04A8";
    static cyrillicCapitalBigYus = "\u046C";
    static cyrillicCapitalEDiaeresis = "\u04EC";
    static cyrillicCapitalKomiDZJE = "\u0506";
    static cyrillicCapitalKsi = "\u046E";
    static cyrillicCapitalLittleYus = "\u0466";
    static cyrillicCapitalOmegaTitlo = "\u047C";
    static cyrillicCapitalShortITail = "\u048A";
    static cyrillicCapitalYat = "\u0462";
    static cyrillicCapitalZheDescender = "\u0496";
    static cyrillicSmalleLigatureAIE = "\u04D5";
    static cyrillicThousands = "\u0482";
    static greekArhaicKoppa = "\u03D8";
    static greekCapitalDottedLunateSigma = "\u03FE";
    static greekCapitalPsi = "\u03A8";
    static greekCapitalReversedDottedLunateSigma = "\u03FF";
    static greekCapitatlOmega = "\u03A9";
    static greekKai = "\u03D7";
    static greekKoppa = "\u03DE";
    static invertedQuestionMark = "\u00BF";
    static latinLambdaStroke = "\u019B";
    static pilcrow = "\u00B6";
    static whiteStar = "\u2606";
}

class KeypadsModule {
    private static columns = [
        [
            // 1
            Symbols.greekArhaicKoppa,
            Symbols.cyrillicCapitalLittleYus,
            Symbols.latinLambdaStroke,
            Symbols.greekKoppa,
            Symbols.cyrillicCapitalBigYus,
            Symbols.greekKai,
            Symbols.greekCapitalReversedDottedLunateSigma
        ],
        [
            // 2
            Symbols.cyrillicCapitalEDiaeresis,
            Symbols.greekArhaicKoppa,
            Symbols.greekCapitalReversedDottedLunateSigma,
            Symbols.cyrillicCapitalAbkhasianHa,
            Symbols.whiteStar,
            Symbols.greekKai,
            Symbols.invertedQuestionMark
        ],
        [
            // 3
            Symbols.copyright,
            Symbols.cyrillicCapitalOmegaTitlo,
            Symbols.cyrillicCapitalAbkhasianHa,
            Symbols.cyrillicCapitalZheDescender,
            Symbols.cyrillicCapitalKomiDZJE,
            Symbols.latinLambdaStroke,
            Symbols.whiteStar
        ],
        [
            // 4
            Symbols.copticCapitalShima,
            Symbols.pilcrow,
            Symbols.cyrillicCapitalYat,
            Symbols.cyrillicCapitalBigYus,
            Symbols.cyrillicCapitalZheDescender,
            Symbols.invertedQuestionMark,
            Symbols.arabicTehRing
        ],
        [
            // 5
            Symbols.greekCapitalPsi,
            Symbols.arabicTehRing,
            Symbols.cyrillicCapitalYat,
            Symbols.greekCapitalDottedLunateSigma,
            Symbols.pilcrow,
            Symbols.cyrillicCapitalKsi,
            Symbols.blackStar
        ],
        [
            // 6
            Symbols.copticCapitalShima,
            Symbols.cyrillicCapitalEDiaeresis,
            Symbols.cyrillicThousands,
            Symbols.cyrillicSmalleLigatureAIE,
            Symbols.greekCapitalPsi,
            Symbols.cyrillicCapitalShortITail,
            Symbols.greekCapitatlOmega
        ]
    ];

    // TODO: Is there another way to make this more like Enumberable<Enumberable<string>>?
    private static columnsEnumerable = [
        Enumerable.From(KeypadsModule.columns[0]),
        Enumerable.From(KeypadsModule.columns[1]),
        Enumerable.From(KeypadsModule.columns[2]),
        Enumerable.From(KeypadsModule.columns[3]),
        Enumerable.From(KeypadsModule.columns[4]),
        Enumerable.From(KeypadsModule.columns[5])
    ];

    private bombProperties: IBombProperties;

    constructor(bombProperties: IBombProperties) {
        this.bombProperties = bombProperties;
    }

    getColumnIndex(symbols: string[]): number {
        const symbolsEnumberable = Enumerable.From(symbols);

        for (let i = 0; i < KeypadsModule.columnsEnumerable.length; i++) {

            const intersection = KeypadsModule.columnsEnumerable[i].Intersect(symbolsEnumberable);

            // if all of the input symbols intersected we have found the column
            if (intersection.Count() === symbolsEnumberable.Count()) {
                return i;
            }
        }

        throw "no symbol columns match symbols";
    }

    getOrderdedSymbols(symbols: string[], columnIndex = 0): string[] {
        const symbolsEnumberable = Enumerable.From(symbols);
        const result: string[] = [];

        // get the column index if it wasn't presented.
        if (columnIndex === 0)
            columnIndex = this.getColumnIndex(symbols);

        for (let i = 0; i < KeypadsModule.columnsEnumerable[columnIndex].Count(); i++) {
            if (symbolsEnumberable.Contains(KeypadsModule.columnsEnumerable[columnIndex][i])) {
                result.push(KeypadsModule.columnsEnumerable[columnIndex][i]);

                // if all of them are mapped then don't worry about looping through the rest.
                if (result.length === symbolsEnumberable.Count())
                    break;
            }
        }
        return result;
    }
}