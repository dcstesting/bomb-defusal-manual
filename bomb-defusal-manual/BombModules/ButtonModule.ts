﻿enum Color {
    Blue,
    Black,
    Yellow,
    Red,
    White,
}

interface IButton {
    color: Color;
    label: string;
}


class ButtonModule {
    private bombProperties: IBombProperties;
    private pressAndHold = "Press And Hold.";
    private pressAndRelease = "Press And Immediatly Release.";

    constructor(bombProperties: IBombProperties) {
        this.bombProperties = bombProperties;
    }

    getInstructions(button: IButton): string {
        const litIndicators = Enumerable.From(this.bombProperties.litIndicators);

        if (button.color === Color.Blue && button.label === "Abort")
            return this.pressAndHold;
        else if (this.bombProperties.batteries.length > 1 && button.label === "Detonate")
            return this.pressAndRelease;
        else if (button.color === Color.White && litIndicators.Any(i => i === "FRK"))
            return this.pressAndHold;
        else if (this.bombProperties.batteries.length > 2 && litIndicators.Any(i => i === "FRK"))
            return this.pressAndRelease;
        else if (button.color === Color.Yellow)
            return this.pressAndHold;
        else if (button.color === Color.Red && button.label === "Hold")
            return this.pressAndRelease;
        else
            return this.pressAndHold;
    }

    getHeldButtonInstructions(stripColor: Color): string {

        // TODO: String.Format ? 
        if (stripColor === Color.Blue)
            return "Release when the countdown timer has a 4 in any position.";
        else if (stripColor === Color.White)
            return "Release when the countdown timer has a 1 in any position.";
        else if (stripColor === Color.Yellow)
            return "Release when the countdown timer has a 5 in any position.";
        else
            return "Release when the countdown timer has a 1 in any position.";
    }
}