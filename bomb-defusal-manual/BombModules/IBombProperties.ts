﻿enum Battery {
    AA,
    D
}

interface IBombProperties {
    serialNumber: string;
    litIndicators: string[];
    batteries: Battery[];
}