﻿/// <reference path="../Scripts/typings/linq/linq.d.ts" />

enum Wire {
    Blue,
    Black,
    Red,
    White,
    Yellow
}

class SimpleWiresModule {

    private bombProperties: IBombProperties;

    constructor(bombProperties: IBombProperties) {
        this.bombProperties = bombProperties;
    }

    getWireToCut(wiresArray: Wire[]): number {
        const wires = Enumerable.From(wiresArray);
        const isSerialNumberOdd = Number(this.bombProperties.serialNumber[0]) % 2 !== 0;
        if (wires.Count() === 3) {

            if (wires.Any(w => w === Wire.Red))
                return 2;
            else if (wires.Last() === Wire.White)
                return 3; // last
            else if (wires.Count(w => w === Wire.Blue) > 1)
                return wires.LastIndexOf(Wire.Blue); // TODO: is this 0 based? 
            else
                return 3; // last

        } else if (wires.Count() === 4) {

            if (wires.Count(w => w === Wire.Red) > 1 && isSerialNumberOdd)
                return wires.LastIndexOf(Wire.Red); // TODO: is this 0 based? 
            else if (wires.Last() === Wire.Yellow && !wires.Any(w => w === Wire.Red))
                return 4; // last
            else if (wires.Count(w => w === Wire.Blue) === 1)
                return 1;
            else if (wires.Count(w => w === Wire.Yellow) > 1)
                return 4; // last
            else
                return 2;

        } else if (wires.Count() === 5) {

            if (wires.Last() === Wire.Black && isSerialNumberOdd)
                return 4;
            else if (wires.Count(w => w === Wire.Red) === 1 && wires.Count(w => w === Wire.Yellow) > 1)
                return 1;
            else if (!wires.Any(w => w === Wire.Black))
                return 2;
            else
                return 1;


        } else if (wires.Count() === 6) {

            if (!wires.Any(w => w === Wire.Yellow) && isSerialNumberOdd)
                return 3;
            else if (wires.Count(w => w === Wire.Yellow) === 1 && wires.Count(w => w === Wire.White) > 1)
                return 4;
            else if (!wires.Any(w => w === Wire.Red))
                return 6;
            else
                return 4;
        }

        throw { name: "NotImplementedError", message: "too lazy to implement" };

    }
}