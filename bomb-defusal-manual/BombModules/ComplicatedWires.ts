﻿/// <reference path="IBombProperties.ts" />

class ComplexWire {

    LED: boolean;
    Red: boolean;
    Blue: boolean;
    Star: boolean;
    InstructionLetter: string;
}

class ComplicatedWiresModule {

    // Truth table.
    private WireCombinations: ComplexWire[] = [
        { LED: false, Red: false, Blue: false, Star: false, InstructionLetter: "C" },
        { LED: false, Red: false, Blue: false, Star: true, InstructionLetter: "C" },
        { LED: false, Red: false, Blue: true, Star: false, InstructionLetter: "S" },
        { LED: false, Red: false, Blue: true, Star: true, InstructionLetter: "D" },
        { LED: false, Red: true, Blue: false, Star: false, InstructionLetter: "S" },
        { LED: false, Red: true, Blue: false, Star: true, InstructionLetter: "C" },
        { LED: false, Red: true, Blue: true, Star: false, InstructionLetter: "S" },
        { LED: false, Red: true, Blue: true, Star: true, InstructionLetter: "P" },
        { LED: true, Red: false, Blue: false, Star: false, InstructionLetter: "D" },
        { LED: true, Red: false, Blue: false, Star: true, InstructionLetter: "B" },
        { LED: true, Red: false, Blue: true, Star: false, InstructionLetter: "P" },
        { LED: true, Red: false, Blue: true, Star: true, InstructionLetter: "P" },
        { LED: true, Red: true, Blue: false, Star: false, InstructionLetter: "B" },
        { LED: true, Red: true, Blue: false, Star: true, InstructionLetter: "B" },
        { LED: true, Red: true, Blue: true, Star: false, InstructionLetter: "S" },
        { LED: true, Red: true, Blue: true, Star: true, InstructionLetter: "D" }
    ];

    private bombProperties: IBombProperties;

    constructor(bombProperties: IBombProperties) {
        this.bombProperties = bombProperties;
    }

    GetWireResult(wire: ComplexWire): string {
        const wireCombos = Enumerable.From(this.WireCombinations);

        const match = wireCombos.FirstOrDefault(w => wire.LED == w.LED && w.Red = wire.Red && w.Blue == wire.Blue && w.Star == wire.Star);

        // TODO: enum instruction letters 
        // TODO: enum or const return values.
        // TODO: consider making one if? if it can be readable or perhaps  case statement? 
        if (match === "C")
            return "Cut";
        else if (match === "S" && Number(this.bombProperties.serialNumber[this.bombProperties.serialNumber.length - 1]) % 2 === 0) // TODO: make local variable from last digit
            return "Cut";
        else if (match === "C" && 1) //TODO: bomb has parallel 
            return "Cut";
        else if (match === "B" && this.bombProperties.batteries.length > 2)
            return "Cut";
        else
            return "Do Not Cut";

    }
}