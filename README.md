# bomb-defusal-manual
A ~~TDD~~ TypeScript implementation of parts of the manual. Warning: I do not recommend playing the game with this or any future product of this as it will likely ruin the fun of the game.  

# Purpose
* An introduction to TypeScript, and a re-introduction into javascript + new tooling.
* Create a interactive website that could be used as an alternative to the manual.
* Kobayashi Maru

![Kobayashi Maru gif](http://i.stack.imgur.com/6ju8O.gif)

# To Do
 Module | Rough Logic | Tests | UI 
-------------  | ----- | ----- | -----
 On the Subject Of Wires | + | - | -
 On the Subject Of The Button | + | - | - 
 On the Subject Keypads | + | - | - 
 On the Subject Of Simon Says | - | - | - 
 On the Subject Of Who's First | - | - | - 
 On the Subject Of Memory | - | - | - 
 On the Subject Of Morse Code | - | - | - 
 On the Subject Of Complicated Wires | - | - | - 
 On the Subject Of Mazes | - | - | - 
 On the Subject Of Passwords | - | - | - 
 On the Subject Of Venting Gas | - | - | - 
 On the Subject Of Capacitor Discharge| - | - | - 
 On the Subject Of Knobs| - | - | - 

* Travis Ci integration? 
* Add pretty links in readme.md.
* Create actual website + blog post and publish.
* Dream - Look into creating a shared open session with SignalR and an api.
* Moar Kobayashi.
* Tests - need to learn Mocha or Jasmine or Something? 


# What does "Rough Logic" mean? 
It means because I'm a perfect developer the code should always work once its implemented. Don't believe me? pleb... Well thats why they created test frameworks to prove what an awesome developer I am. See "Tests Section"

# Tests
* Coming Soon :)
